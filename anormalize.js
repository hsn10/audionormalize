/* Single file FFmpeg based audio normalizer */

/*
MIT License

Copyright (c) 2023 Radim Kolar

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice (including the next paragraph) shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import yargs from 'yargs'
import { spawn } from 'child_process'
import { existsSync } from 'fs'
import process from "node:process"

/**
 EBUR QA check

 https://www.pro-tools-expert.com/home-page/ebu-loudness-standard-updated
*/
const QA_CHECK_LU = 0.2

/**
Encoded audio bitrate in kbits
*/
const AUDIO_BITRATE = 160

/**
 Output audio codec table based on file extension
*/
const codec_table = { "m4a": "alac", "wav" : "pcm_f32le", "mp4":"libfdk_aac", "webm":"libopus", "weba":"libopus" }

/*
  ffmpeg -hide_banner -i "putin-wine.mp4" 
    -af adeclip,alimiter=-0.6dB,loudnorm=I=-14:TP=-1:print_format=summary:offset=+0.7,
     aresample=48k:dither_method=triangular_hp 
    -vcodec copy -acodec libfdk_aac -y "putin_wine_2.mp4"
*/

/**
 * Returns path to ffmpeg binary
*/
export function getFFmpeg() {
   const ENV_VAR = 'FFMPEG'
      function defaultFallback() {
         const H1 = 'HOMEDRIVE'
         const H2 = 'HOMEPATH'
         if (process.env[H1] && process.env[H2]) {
           return process.env[H1]+process.env[H2]+"\\Downloads\\ffmpeg.exe"
         }
         return null
      }

   let path = process.env[ENV_VAR]
   if (path && existsSync(path)) return path
   path = defaultFallback()
   if (path && existsSync(path)) return path
   return 'ffmpeg'
}


/**
  Get arguments from command line
  Checks for supported file extensions
*/
function getCommandLineArguments() {
   function checkArgs(argv) {
      if (argv._.length < 2 ) return 'You need to specify at least input and output file'
      if (argv._.length > 2 ) return 'You need to specify ONLY ONE input and output file pair'
      if (argv._[0] === argv._[1] ) return 'input and output file must be different'
      const outputfn = argv._.at(-1).toLowerCase()
      if (! (outputfn.slice(outputfn.lastIndexOf('.')+1) in codec_table)) return 'Unsupported output file extension. Supported extensions are: ' + Object.keys(codec_table)
      if ( argv.I > -5.0 || argv.I < -70.0 ) return 'ILU target must be -70 to -5'
      if ( argv.T > 0.0 || argv.T <  -9.0 )  return 'True Peaks target must be between -9 and 0'
      if ( argv.m <= 0 ) return 'Max iteration must be positive non zero number'

      return true
   }
   const argv = yargs(process.argv.slice(2))
      .parserConfiguration({"parse-numbers": true, "parse-positional-numbers": false })
      .usage("node anormalize.js [-m maxiterations] [ -I=-LU ] [ -T=-dbTP ] [ -d ] <input> <output>")
      .version("1.0.0")
      .alias('h', 'help')
      .alias('v', 'version')
      .option('I', { boolean: false, alias: ['ILU','i'], default: -14.0, description: "Integrated LoUdness target" } )
      .option('d', { boolean: true, default: false, description: "Force dynamic normalization" } )
      .option('T', { boolean: false, alias: ['TP','t'], default: -1.0 } )
      .option('m', { number: true, default: 4, description: "Maximum number of iterations" } )
      .check(checkArgs)
      .parse()
   
   return [ argv._.slice(0,2) , argv ]
}

/**
  Build arguments for checking if file has an audio track
*/
function buildAudioTrackArguments(inputfile) {
  return ['-hide_banner', '-i', inputfile, '-map', '0:a',
    '-aframes', 1, '-f', 'null', '-' ]
}


/**
  Build arguments for initial loudness analysis
*/
function buildAnalyseArguments(inputfile,i,tp) {
  return ['-hide_banner', '-i', inputfile, '-map', '0:a', '-af',
    `loudnorm=I=${i}:TP=${tp}:print_format=summary`, '-f', 'null', '-' ]
}

/**
  Build arguments for normalizing audio
*/
function buildNormalizeArguments(files, i, tp, analysis) {
  return ['-hide_banner', '-i', files[0], '-af',
    `adeclip,loudnorm=I=${i}:TP=${tp}:measured_I=${analysis.i}:measured_tp=${analysis.tp}:measured_lra=${analysis.lra}:measured_thresh=${analysis.thr}:offset=${analysis.offset}:print_format=summary,aresample=48k:dither_method=triangular_hp`,
    '-vcodec', 'copy', '-acodec', codec_table[files[1].toLowerCase().slice(files[1].lastIndexOf('.')+1)], '-b:a', AUDIO_BITRATE+'k',
    '-movflags', '+faststart',
    files[1], '-y' ]
}

/**
  Build arguments for copying video
*/
function buildCopyArguments(files) {
  return ['-hide_banner', '-i', files[0], '-map', '0:v', '-vcodec', 'copy',
           files[1], '-y' ]
}

/**
  Check if analysis indicates not empty audio track
*/
function haveAudio(analysis) {
   if (analysis.lra === 0.0 && analysis.thr === -70.0) return false
   return true
}

/**
 Check if child exited succesfully.
 returns Promise
*/
function checkChild(child) {
  return new Promise( (resolve, reject) => {
     child.on('exit', (code, signal) => {
     if (code) {
       console.error('Child exited with code', code)
       resolve(false)
     } else if (signal) {
       console.error('Child was killed with signal', signal);
       reject(signal)
     } else {
       resolve(true)
     }
  }); // end .on

  }) // end Promise
}

export async function normalizeFile( FFMPEG, files, I, TP, m ) {
  // Step 1 - check if input file have audio track
  const trackCheck = spawn(FFMPEG, buildAudioTrackArguments(files[0]))
  const audioTrack = await checkChild(trackCheck)
  // Step 2 - analyse input file
  if (audioTrack) {
     const analysis = spawn(FFMPEG, buildAnalyseArguments(files[0],I,TP))
     console.log(`Analysing input ${files[0]}`)
     var analysis_rc = await parseFFmpegOutput(analysis,files[0])
  }
  if (! audioTrack || ! haveAudio(analysis_rc) ) {
     console.log("No audio found, copying video track to output.")
     spawn(FFMPEG, buildCopyArguments(files))
     return
  }

  let optsteps = m
  let perfect = false
  let bestcorrection = 0.0
  let bestdelta = Number.MAX_VALUE

  let oldoffset = 0.0
  let odelta = analysis_rc.offset
  while( optsteps > 0 ) {
     oldoffset = oldoffset + odelta
     analysis_rc.offset = oldoffset
     console.log(`Normalizing ${files[0]} with corrective offset ${oldoffset.toFixed(1)}, step ${m-optsteps+1}/${m}.`)
     const normalize = spawn(FFMPEG, buildNormalizeArguments(files, I, TP, analysis_rc))
     analysis_rc = await parseFFmpegOutput(normalize,files[0])

     odelta = analysis_rc.offset
     if (Math.abs(odelta) < bestdelta) {
        bestdelta = Math.abs(odelta)
        bestcorrection = oldoffset
     }
     optsteps = optsteps - 1
     /* exit if delta is within EBU-R QA check */
     if ( Math.abs(odelta) <= QA_CHECK_LU ) {
       perfect = true
       break
     }
  }
  if ( perfect === false ) {
     console.log(`Can't normalize ${files[0]} to <=${QA_CHECK_LU} target offset within ${m} max steps.\nApplying best known correction ${bestcorrection} offset will be ${bestdelta}`)
     analysis_rc.offset = bestcorrection
     spawn(FFMPEG, buildNormalizeArguments(files, I, TP, analysis_rc))
  }
}

/**
  Program Entry point for anormalize
*/
async function main() {
  const [files, args] = getCommandLineArguments()
  const FFMPEG = getFFmpeg()
  normalizeFile( FFMPEG, files, args.I, args.TP, args.m )
}

/*
Input Integrated:   -15.2 LUFS
Input True Peak:     -3.3 dBTP
Input LRA:            3.4 LU
Input Threshold:    -25.3 LUFS
Target Offset:        +1.2 LU
Normalization Type:   Dynamic
Output LRA:            5.9 LU
Output Integrated:   -14.8 LUFS
*/
function parseFFmpegOutput(process,filename) {
  const ilurx     = /^Input Integrated:\s+(-[.0-9]+)/m
  const tprx      = /^Input True Peak:\s+([-+]?[.0-9]+)/m
  const lrarx     = /^Input LRA:\s+([.0-9]+)/m
  const threshrx  = /^Input Threshold:\s+(-[.0-9]+)/m
  const offsetrx  = /^Target Offset:\s+([-+]?[.0-9]+)/m
  const iluoutrx  = /^Output Integrated:\s+(-[.0-9]+)/m
  const lraoutrx  = /^Output LRA:\s+([.0-9]+)/m
  const typerx    = /^Normalization Type:\s+([a-zA-Z]+)/m

  return new Promise( (resolve, reject) => {
  var ilu = null
  var tp = null
  var lra = null
  var thr = null
  var offset = null
  var ntype = null
  var printed_header = false

  const printHeader = function () {
     if (! printed_header ) {
        console.log(`Analysed ${filename}:`)
        printed_header = true
     }
  }
  
  process.stderr.on('data', (data) => {
     // console.log(`stderr: ${data}`); // COMMENT DEBUG HERE
     const i = ilurx.exec(data)
     if (i) {
        printHeader()
        ilu = i[1]
        console.log(`in ilu: ${ilu}`)
     }
     const io = iluoutrx.exec(data)
     if (io) {
        console.log(`out ilu: ${io[1]}`)
     }
     const t = tprx.exec(data)
     if (t) {
        tp = t[1]
        console.log(`in tp: ${tp}`)
     }
     const l = lrarx.exec(data)
     if (l) {
        lra = Number(l[1])
        console.log(`in lra: ${lra}`)
     }
     const lo = lraoutrx.exec(data)
     if (lo) {
        console.log(`out lra: ${lo[1]}`)
     }

    const h = threshrx.exec(data)
    if (h) {
        thr = Number(h[1])
        console.log(`thresh: ${thr}`)
     }
    const o = offsetrx.exec(data)
    if (o) {
        offset = Number(o[1])
        console.log(`offset: ${offset}`)
     }
    const nt = typerx.exec(data)
    if (nt) {
        ntype = nt[1]
        console.log(`normalizer: ${ntype}`)
    }
  })
  process.on('error', (err) => {
      reject(`ffmpeg error ${err}`)
  })
  process.on('exit', (code) => {
     if(code != 0) {
       console.log(`ffmpeg process exited with err code ${code}`)
       reject(`ffmpeg process exited with err code ${code}`)
     } else {
       resolve ( { i: ilu, tp: tp, lra: lra, thr: thr, offset: offset, mode: ntype } )
     }
  })
  })
}

import path from 'path';
import { fileURLToPath } from 'url'

function isRunningDirectlyViaCLI() {
   const nodePath = path.resolve(process.argv[1]);
   const modulePath = path.resolve(fileURLToPath(import.meta.url))
   return nodePath === modulePath
}

if (isRunningDirectlyViaCLI()) {
   /* invoked from CLI, run main() */
   console.log(`FFmpeg detected at ${getFFmpeg()}`)
   main()
}
