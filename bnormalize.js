/* Multiple (batch) file FFmpeg based audio normalizer */

/*
MIT License

Copyright (c) 2023 Radim Kolar

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice (including the next paragraph) shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import { globSync } from 'glob'
import yargs from 'yargs'

const NORM_SUFFIX = "-NN"

/**
  Get arguments from command line
  Checks for supported file extensions
*/
function getCommandLineArguments() {
   /** check command line arguments for validity */
   function checkArgs(argv) {
      if ( argv.m <= 0.0 ) return 'Max iterations must be positive non zero number'
      if ( argv.m <  1.0 ) return 'Minimum number of max iterations is one.'
      return true
   }

   const argv = yargs(process.argv.slice(2))
      .parserConfiguration({"parse-numbers": false, "parse-positional-numbers": false })
      .usage("node bnormalize.js [-m maxiterations] <file glob> ...")
      .version("1.1.0")
      .alias('h', 'help')
      .alias('v', 'version')
      .option('m', { number: true, default: 4, description: "Maximum number of iterations" } )
      .check(checkArgs)
      .parse();

   // glob files
   return [ globSync(argv._, { }) , argv ]
}

/**
  Generate target file name
*/
function generateTarget(input) {
   // find last "."
   const i = input.lastIndexOf(".")
   if ( i === -1 ) {
      return input + "." + NORM_SUFFIX
   } else {
      return input.slice(0,i) + NORM_SUFFIX + input.slice(i)
   }
}

import { normalizeFile, getFFmpeg } from './anormalize.js' // ( FFMPEG, files, I, TP, m )

/**
  Program Main Entry point for bnormalize
*/
async function main() {
   const [ files, argv ] = getCommandLineArguments()
   // if already processed file exists, remove source
   const pass1 = files.filter( item => (! files.includes(generateTarget(item)) ) )
   // remove already processed files
   const pass2 = pass1.filter( item => (! item.includes(NORM_SUFFIX+".") ) )
   // console.log(pass2)
   const FFMPEG = getFFmpeg()
   pass2.forEach( item => {
      const t = generateTarget(item);
      normalizeFile( FFMPEG, [ item, t ], -14.0, -1.0, argv.m )
   })
   // console.log(work, args )
   
}

main();
